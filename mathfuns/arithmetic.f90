module artithmetic_module
   implicit none

contains

   real function add(x, y) result (sum)
      implicit none
      real, intent(in) :: x
      real, intent(in) :: y

      sum = x + y

   end function add

   real function sub(x, y) result (diff)
      implicit none
      real, intent(in) :: x
      real, intent(in) :: y

      diff = x - y

   end function sub

   real function mul(x, y) result (prod)
      implicit none
      real, intent(in) :: x
      real, intent(in) :: y

      prod = x * y

   end function mul

end module artithmetic_module
