# Fortran-CMake-UnitTest-CI

A small example that combines Fortran, CMake, pFUnit (unit-testing framework), and GitLab CI.

## Introduction

This project contains a simple `arithmetic_module` located in the `mathfuns/arithmetich.f90` file with 3 functions `add()`, `sub()`, and `mul()`.
To build the project, we first use the [CMake](https://cmake.org/) to generate the build files (most commonly, the Makefiles).
Furthermore, we use CMake to integrate the unit-testing framework called [pFUnit](https://github.com/Goddard-Fortran-Ecosystem/pFUnit) through `git submodules`.
The unit tests are placed in the `tests/` directory.
On top of that, the project uses GitLab CI to run tests after each new push.

## Dependencies

This project requires:

* Fortran compiler
* [git](https://git-scm.com/)
* [CMake](https://cmake.org/)
* [Python 3](https://www.python.org)

## Running the project

To run locally the project and tests just execute the script:

```sh
./build_and_test.sh
```

This script:

1. Downloads dependencies from other git repositories:
```sh
git submodule update --init --recursive
```
2. Builds the pFUnit framework (which doesn't build properly even though it is included in the main project with the `add_subdirectory()` directive):
```sh
cd third-party/pFUnit
cmake CMakeLists.txt
make -j 16
```
3. Builds the main project:
```sh
cd ../../
cmake CMakeLists.txt
make -j 16
```
4. And runs the unit tests using CTest from CMake:
```sh
cd tests/
ctest
```
