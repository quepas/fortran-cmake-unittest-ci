#!/bin/sh
# Get the dependencies
git submodule update --init --recursive
# Compile the pFUnit manually (inclusion through add_subdirectories() doesn't build it properly)
cd third-party/pFUnit
cmake CMakeLists.txt
make -j 16
# Build the main project
cd ../../
cmake CMakeLists.txt
make -j 16
# Run tests
cd tests/
ctest

